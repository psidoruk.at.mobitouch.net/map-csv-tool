import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class MapCsvTool {
  private static final String COMMA = ",";
  private static final String NEW_LINE = "\r\n";

  public static void creteCsvFile(Map<String, Integer> textNumberMap, String destinationPath) throws IOException {
    createFileIfNotExists(destinationPath);
    BufferedWriter writer = new BufferedWriter(new FileWriter(destinationPath));
    textNumberMap.forEach((key, value) -> {
      try {
        writer
            .append(key)
            .append(COMMA)
            .append(value.toString())
            .append(NEW_LINE);
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
    writer.close();
  }

  private static void createFileIfNotExists(String destinationPath) throws IOException {
    File file = new File(destinationPath);
    file.getParentFile().mkdirs();
    file.createNewFile();
  }

}